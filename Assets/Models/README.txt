Models:
Where all the 3D assets, especially models are stored

This folder contains the following folders:

Animations:
Where all the animations are stored

Textures:
Where all the images specifically used for texturing 3D models are stored.
Putting textures here makes it easier for the auto-generated materials to find its corresponding textures.

Materials:
Where all the materials used in models are used.
Auto-generated material are created here.
